from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import Paragraph


class MultiFontParagraph(Paragraph):
    # Created by B8Vrede for http://stackoverflow.com/questions/35172207/
    def __init__(self, text, style, fonts_locations):

        font_list = []
        for font_name, font_location in fonts_locations:
            # Load the font
            font = TTFont(font_name, font_location)

            # Get the char width of all known symbols
            font_widths = font.face.charWidths

            # Register the font to able it use
            pdfmetrics.registerFont(font)

            # Store the font and info in a list for lookup
            font_list.append((font_name, font_widths))

        # Set up the string to hold the new text
        new_text = u''

        # Some variable for efficiency
        previous_font = None
        tag = False

        # Loop through the string
        for char in text:

            if char == "<":
                tag = True

            if tag:
                if char == ">":
                    tag = False

                new_text += char
                continue

            # Loop through the fonts
            for font_name, font_widths in font_list:

                # Check whether this font know the width of the character
                # If so it has a Glyph for it so use it
                if ord(char) in font_widths:

                    if previous_font == font_name:
                        new_text += u'{}'.format(char)
                    elif previous_font is None:
                        new_text += u'<font name="{}">{}'.format(font_name, char)
                    else:
                        new_text += u'</font><font name="{}">{}'.format(font_name, char)

                    previous_font = font_name
                    # Set the working font for the current character
                    break

        new_text += u'</font>'
        print new_text
        Paragraph.__init__(self, new_text, style)